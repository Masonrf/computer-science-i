// Bobby Love
// October 11, 2022
// GNU GPLv3

// Compile program with the following command: gcc -lm main.c -std=c11 -Wall -Werror
// Execute program with the following command: ./a.out

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 8

int findColMax(int[][SIZE], int, int);

float findColMean(int[][SIZE], int, int);

void initializeArray(int[][SIZE], int, int);

void printArray(int[][SIZE], int, int);

int main() {
    srand(time(NULL));
    int column = 0;
    int max = 0;
    float mean = 0.0;
    int array[SIZE][SIZE] = {0};
    initializeArray(array, SIZE, SIZE);
    puts("Column Analyzer");
    // Will repeat until a valid column value is read.
    do {
        printArray(array, SIZE, SIZE);
        printf("Column Selection: ");
        scanf("%d", &column);
    } while (column < 0 || column > SIZE - 1);
    max = findColMax(array, SIZE, column);
    printf("Column %d Maximum: %d\n", column, max);
    mean = findColMean(array, SIZE, column);
    printf("Column %d Minimum: %.1f\n", column, mean);
    puts("Farewell!");
    return 0;
}

int findColMax(int array[][SIZE], int rows, int column) {
    int max = 0;
    for (int i = 0; i < rows; ++i) {
        if (array[i][column] > max) {
            max = array[i][column];
        }
    }
    return max;
}

float findColMean(int array[][SIZE], int rows, int column) {
    int sum = 0;
    float mean = 0;
    for (int i = 0; i < rows; ++i) {
        sum += array[i][column];
    }
    mean = (float)sum / (float)rows;
    return mean;
}

void initializeArray(int array[][SIZE], int rows, int cols) {
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            array[i][j] = rand() % 10;
        }
    }
}

void printArray(int array[][SIZE], int rows, int cols) {
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            printf("%d", array[i][j]);
            if (j == cols - 1) {
                printf("\n");
            } else {
                printf(" ");
            }
        }
    }
}