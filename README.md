# Computer Science I

## Preamble
This repository contains code assignments and examples similar to those I was assigned in my introductory university computer science class. Coding is perhaps the most rewarding activity I have ever done, and I encourage you to learn it too. Try to complete the following assignments and compare your solutions to mine; if you can successfully complete the following exercises, congratulations! You have equivalent knowledge of a university student who has completed an introductory programming class. Coding becomes easier with practice, so even if you initially find these problems difficult, keep trying! Your understanding will improve with time. Good luck!

## Assignments

### [1: Simple Calculator](simple-calculator/README.md)

### [2: Pythagorean Theorem](pythagorean-theorem/README.md)

### [3: GPA Calculator](gpa-calculator/README.md)

### [4: Electricity Biller](electricity-biller/README.md)

### [5: Array Analyzer](array-analyzer/README.md)

### [6: Column Analyzer](column-analyzer/README.md)

### [7: Array Manipulator](array-manipulator/README.md)

### [8: Flight Reservation](flight-reservation/README.md)

### [9: Fibonacci Calculator](fibonacci-calculator/README.md)

### [10: Bubble Sort](bubble-sort/README.md)

### [11: Caesar Cipher](caesar-cipher/README.md)

### [12: Account Finder](account-finder/README.md)

### [13: City Printer](city-printer/README.md)

## Environment
The code in this repository was written, ran, debugged, and published on a PC running [Arch Linux](https://archlinux.org/). Understanding how to operate UNIX-like systems is a worthwhile skill for the aspiring computer scientist, so I highly recommend trying a simple, user-friendly flavor of [Linux](https://en.wikipedia.org/wiki/Linux), such as [Ubuntu](https://ubuntu.com/) for the development of these projects. Both choices are free and open source, so you have nothing to lose, and much to gain! As for choice of IDE, I use and recommend [Visual Studio Code](https://code.visualstudio.com/), a powerful text editor available on all common Linux distributions.

## Credits
[Jim Ries](https://engineering.missouri.edu/faculty/jim-ries/) was the individual who taught me how to write code and prescribed the assignments that would become the basis of this repository. Passing his class wasn't easy, but the office hours he provided (during which he was immensely patient) were enlightening and allowed me to not only succeed, but also find the passion for programming I have today; I will never forget what he has done for me. In case you're reading this JimR: thank you for everything.

## License
GNU GPLv3
