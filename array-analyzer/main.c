// Bobby Love
// October 10, 2022
// GNU GPLv3

// Compile program with the following command: gcc -lm main.c -std=c11 -Wall -Werror
// Execute program with the following command: ./a.out

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
// time.h allows for the use of time based functions, such as time().
#include <time.h>

// #define is useful for reoccurring numbers that are found in many scopes.
#define SIZE 16

void fillArray(int[], int);

float meanArray(int[], int);

void printArray(int[], int);

float stdDeviation(int[], int);

// Notice how most code is not found in main(), but rather the functions defined below main().
// Generally, good software design calls for main() to be used as a driver function to call other functions.
int main() {
    // Responsible for seeding the random number generator.
    srand(time(NULL));
    // Creates an array with 100 indexes, all initialized to 0.
    int array[SIZE] = {0};
    puts("Array Analyzer");
    fillArray(array, SIZE);
    printArray(array, SIZE);
    printf("Mean: %.3f\n", meanArray(array, SIZE));
    printf("Standard Deviation: %.3f\n", stdDeviation(array, SIZE));
    puts("Farewell!");
    return 0;
}

void fillArray(int array[], int size) {
    for (int i = 0; i < size; ++i) {
        // Ensures that any number rand() returns will be reduced to 9 or less.
        array[i] = rand() % 10;
    }
}

float meanArray(int array[], int size) {
    int sum = 0;
    float mean = 0.0;
    for (int i = 0; i < size; ++i) {
        sum += array[i];
    }
    // Cast the operands to floats to ensure no data is truncated.
    mean = (float)sum / (float)size;
    return mean;
}

void printArray(int array[], int size) {
    printf("Array: ");
    for (int i = 0; i < size; ++i) {
        printf("%d", array[i]);
        if (i != size - 1) {
            printf(" ");
        } else {
            printf("\n");
        }
    }
}

float stdDeviation(int array[], int size) {
    float stdDev = 0.0;
    float mean = meanArray(array, size);
    float summation = 0.0;
    float fraction = 0.0;
    for (int i = 0; i < size; ++i) {
        summation += pow(array[i] - mean, 2);
    }
    fraction = summation / (float)size;
    stdDev = sqrt(fraction);
    return stdDev;
}