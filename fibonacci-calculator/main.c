// Bobby Love
// October 13, 2022
// GNU GPLv3

// Compile program with the following command: gcc -lm main.c -std=c11 -Wall -Werror
// Execute program with the following command: ./a.out

#include <math.h>
#include <stdio.h>
#include <time.h>

#define SIZE 32

int iterativeFib(int);

int recursiveFib(int);

void printArray(int[], int);

void printLine(void);

void printMenu(void);

void reverseArray(int[], int);

int main() {
    int array[SIZE] = {0};
    int choice = 0;
    float time = 0.0;
    clock_t start = 0;
    clock_t end = 0;
    do {
        printMenu();
        scanf("%d", &choice);
        if (choice == 1) {
            printLine();
            puts("Iterating...");
            start = clock();
            for (unsigned int i = 0; i < SIZE; ++i) {
                // array[i] = iterativeFib(i);
                *(array + i) = iterativeFib(i);
            }
            end = clock();
            time = (float)(end - start) / (float)CLOCKS_PER_SEC;
            printLine();
            printf("Time Elapsed: %f s\n", time);
        } else if (choice == 2) {
            printLine();
            puts("Recursing...");
            start = clock();
            for (unsigned int i = 0; i < SIZE; ++i) {
                // array[i] = recursiveFib(i);
                *(array + i) = recursiveFib(i);
            }
            end = clock();
            time = (float)(end - start) / (float)CLOCKS_PER_SEC;
            printLine();
            printf("Time Elapsed: %f s\n", time);
        } else if (choice == 3) {
            printLine();
            puts("Printing array...");
            printArray(array, SIZE);
        } else if (choice == 4) {
            printLine();
            puts("Reversing array...");
            reverseArray(array, SIZE);
        } else if (choice == 5) {
            printLine();
            puts("Farewell!");
            printLine();
            break;
        } else {
            printLine();
            puts("Invalid option selected, try again.");
            continue;
        }
    } while (1);
    return 0;
}

// This function uses iteration, as it never calls itself.
// Iteration is usually more performant than recursion, so always use it, when possible.
int iterativeFib(int input) {
    if (!input) {
        return 0;
    }
    int prev;
    int curr = 0;
    int next = 1;
    for (int i = 1; i < input; ++i) {
        prev = curr;
        curr = next;
        next = prev + curr;
    }
    return next;
}

// This function uses tail recursion, which means the function calls itself at its tail.
// Tail recursion is more efficient than traditional recursion.
int recursiveFib(int input) {
    if (!input) {
        return 0;
    } else if (input == 1) {
        return 1;
    }
    return recursiveFib(input - 1) + recursiveFib(input - 2);
}


void printArray(int array[], int length) {
    printLine();
    for (int i = 0; i < length; ++i) {
        printf("%d", *(array + i));
        if (!((i + 1) % 4)) {
            puts("");
        } else {
            printf(" ");
        }
    }
}

void printLine(void) {
    puts("================================================================");
}

void printMenu(void) {
    printLine();
    puts("Fibonacci Calculator");
    printLine();
    puts("(1): Iterative Fibonacci");
    puts("(2): Recursive Fibonacci");
    puts("(3): Print Array");
    puts("(4): Reverse Array");
    puts("(5): Exit");
    printLine();
    fputs("Selection: ", stdout);
}

void reverseArray(int array[], int length) {
    int hold = 0;
    // Need to stop halfway, else reversal will be undown.
    for (int i = 0; i < length / 2; ++i) {
        // hold = array[i];
        hold = *(array + i);
        // array[i] = array[length - i - 1];
        *(array + i) = *(array + length - i - 1);
        // array[length - i - 1] = hold;
        *(array + length - i - 1) = hold;
    }
}