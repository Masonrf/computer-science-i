# Bubble Sort
- Create a 1D array with 8 columns.
- Fill the array with random values.
    - 0 <= Element <= 9
- Print the value of each index of the unsorted array.
- Sort the array with [bubble sort](https://en.wikipedia.org/wiki/Bubble_sort).
- Print the value of each index of the sorted array.
- Find and print the minimum, median, and maximum of the array.
    - These are easy to find in a sorted array.
